import re

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def create_datalist(data):
    datalist = []
    for i in range (0 , len(data)):
        if i % 4 == 0 :
            typex = ''.join( c for c in data[i+3] if  c not in "'>" )
            temp = { 'value' : data[i+1] , 'type' : typex }
            datalist.append(temp)
    return datalist
def create_codelist(code):
    codelist = []
    code = code.strip().split("\n")
    for i in code:
        i = re.sub(
           r'\s+', 
           ',', 
           i
        )
        i = re.sub(
           r',+', 
           ',', 
           i
        )
        temp = i.split(',')
        tempcode = []
        for j in temp:
            if j == '':
                pass
            else :
                tempcode.append(j)
        codelist.append(tempcode)
    return codelist
def label_match(strg, search=re.compile(r'label-[0-9]+').search):
    return bool(search(strg))
def special_match(strg, search=re.compile(r'M\[[0-9]+\]').search):
    return bool(search(strg))

def label_list(code):
    labellist = []
    for i in range(0 , len(code)):
        if(label_match(str(code[i][0]))):
            labellist.append({'name':code[i][0] , 'line' : i})
    return labellist

def runassembly(code,data,lablelist):
    comp = None
    z = 0
    while(True):
        i = code[z]
        z += 1
        if i[0] == 'CHOP':
            if data[int(find_between(i[2],'[',']'))]['type'] == 'str':
                print("err")
            elif data[int(find_between(i[2],'[',']'))]['type'] == 'int':
                data[int(find_between(i[2],'[',']'))]= data[int(find_between(i[1],'[',']'))]
            elif data[int(find_between(i[2],'[',']'))]['type'] == 'float':
                data[int(find_between(i[2],'[',']'))]['value'] = round(data[int(find_between(i[1],'[',']'))]['value'],0)
                data[int(find_between(i[2],'[',']'))]['type'] = 'int'
        elif i[0] == 'DIV':
            data[int(find_between(i[1],'[',']'))]['value']  = float(data[int(find_between(i[2],'[',']'))]['value']) / float(data[int(find_between(i[3],'[',']'))]['value'])
        elif i[0] == 'SUB':
            data[int(find_between(i[1],'[',']'))]['value']  = float(data[int(find_between(i[2],'[',']'))]['value']) - float(data[int(find_between(i[3],'[',']'))]['value'])
        elif i[0] == 'ADD':
            data[int(find_between(i[1],'[',']'))]['value']  = float(data[int(find_between(i[2],'[',']'))]['value']) + float(data[int(find_between(i[3],'[',']'))]['value'])
        elif i[0] == 'MUL':
            data[int(find_between(i[1],'[',']'))]['value']  = float(data[int(find_between(i[2],'[',']'))]['value']) * float(data[int(find_between(i[3],'[',']'))]['value'])
        elif i[0] == 'MOV':
            data[int(find_between(i[1],'[',']'))]  = data[int(find_between(i[2],'[',']'))]
        elif i[0] == 'POW':
            if(special_match(i[2]) and special_match(i[3])):
                data[int(find_between(i[1],'[',']'))]['value']  = int(float(data[int(find_between(i[2],'[',']'))]['value'])) ** int(float(data[int(find_between(i[3],'[',']'))]['value']))
            elif special_match(i[3]) :
                data[int(find_between(i[1],'[',']'))]['value']  = int(float(i[2])) ** int(float(data[int(find_between(i[3],'[',']'))]['value']))
            elif special_match(i[2]) :
                data[int(find_between(i[1],'[',']'))]['value']  = int(float(data[int(find_between(i[2],'[',']'))]['value'])) ** int(float(i[3]))
            else:
                data[int(find_between(i[1],'[',']'))]['value']  = int(float(i[2])) ** int(float(i[3]))

        elif i[0] == 'INPUT':
            string = ''
            for j in range(1,len(i)):
                if (special_match(i[j])):
                    x = input(string + ': ')
                    data[int(find_between(i[j],'[',']'))]['type'] = 'str'
                    data[int(find_between(i[j],'[',']'))]['value'] = x
                    print(data[int(find_between(i[j],'[',']'))]['value'])
                else:
                    string += ' '+i[j]
        elif i[0] == 'LEN':
            data[int(find_between(i[2],'[',']'))]['value']  = len(data[int(find_between(i[1],'[',']'))]['value'])
        elif i[0] == 'PRINT':
            if(special_match(i[1])):
                print(data[int(find_between(i[1],'[',']'))]['value'])
            else:
                for f in range (1 , len(i)):
                    if(special_match(i[f])):
                        print(data[int(find_between(i[f],'[',']'))]['value'])
                    else:
                        print(i[f])
        elif i[0] == 'SUBSTR':
            x = i[3]
            x = x.split(':')
            data[int(find_between(i[1],'[',']'))]['value']  = str(data[int(find_between(i[2],'[',']'))]['value'])[int(x[0]):int(x[1])] 
        elif i[0] == 'cmp':
            if(float(data[int(find_between(i[1],'[',']'))]['value']) > float(data[int(find_between(i[2],'[',']'))]['value'])):
                comp = 1
            elif(float(data[int(find_between(i[1],'[',']'))]['value']) == float(data[int(find_between(i[2],'[',']'))]['value'])):
                comp = 0
            else:
                comp = -1
        elif i[0] == 'jmp':
            for f in lablelist:
                if f['name'] == i[1]:
                    z = f['line']  + 1     
        elif i[0] == 'jne':
            if(comp != 0):
                comp = None
                for  f in lablelist:
                    if f['name'] == i[1]:
                        z = f['line']  + 1
        elif i[0] == 'je':
            if(comp == 0):
                comp = None
                for  f in lablelist:
                    if f['name'] == i[1]:
                        z = f['line'] + 1      
        elif i[0] == 'jb':
            if(comp > 0):
                comp = None
                for  f in lablelist:
                    if f['name'] == i[1]:
                        z = f['line']  + 1
        if(z == len(code)):
            break


    
fileout = open("out.txt")
text = fileout.read()
data = find_between(text , '.DATA' , '.CODE').split()
code = find_between(text , '.CODE' , '.END')
datalist = create_datalist(data)
codelist = create_codelist(code) 
labellist = label_list(codelist)
runassembly(codelist,datalist,labellist)

tokens = ['CHOP' , 'MEM' , 'MUL' , 'MOV' , 'INPUT' , 'LEN' ,\
'PRINT' , 'SUBSTR' , 'JMP' , 'JB' , 'LABLE' , 'JNE' , 'SUB']

t_CHOP = r'CHOP'
t_MUL = r'MUL'
t_MOV = r'MOV'
t_INPUT = r'INPUT'
t_LEN = r'LEN'
t_PRINT = r'PRINT'
t_SUBSTR = r'SUBSTR'
t_JMP = r'jmp'
t_JB = r'jb'
t_JNE = r'jne'
t_SUB = r'SUB'

def t_MEM(t):
    r'M\[[0-9]+\]'
    print(t_MEM.__doc__, t.value)
def t_LABLE(t):
    r'lable-[0-9]+'
    print(t_LABLE.__doc__, t.value)
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
t_ignore = "\t | \n"

import ply.lex as lex

lex.lex()
