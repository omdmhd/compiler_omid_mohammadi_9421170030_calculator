from code_data import symbolTable, Label

reserved = {
   'if' : 'IF',
   'else' : 'ELSE',
   'while' : 'WHILE',
}

tokens = ['NUMBER', 'PLUS', 'MINUS', 'DIV', 'MUL', 'OPENP', 'ENDP', 'POW', 'LCURLY',\
 'RCURLY', 'BIGGER', 'LESS', 'EQUAL', 'NOTEQUAL', \
  'ID', 'STRING', 'ASSIGN', 'SEMICOLON', 'COMMA', 'LBRAC', 'RBRAC', 'COLON', 'COMMENT'] + list(reserved.values())

t_PLUS    = r'\+'
t_MINUS    = r'\-'
t_DIV    = r'\/'
t_MUL    = r'\*'
t_POW  = r'\^'
t_LCURLY = r'\{'
t_RCURLY = r'\}'
t_BIGGER = r'\>'
t_LESS = r'\<'
t_EQUAL = r'\='
t_NOTEQUAL = r'\<\>'
t_LBRAC = r'\['
t_RBRAC = r'\]'
t_COLON = r'\:'
t_ELSE = r'else'
t_ASSIGN = r'\:='
t_SEMICOLON = r'\;'
t_COMMA = r'\,'

def t_NUMBER(t):
    r'[0-9]+(\.[0-9]+)?'
    #print(t_NUMBER.__doc__, t.value)
    print(t.value)
    t.value = float(t.value)
    return t

def t_ID(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    #print(t_ID.__doc__, t.value)
    t.value = t.value
    print(reserved.get(t.value,'ID') )
    t.type = reserved.get(t.value,'ID')    # Check for reserved words
    return t

def t_STRING(t):
    r'''[\"|\'][A-z-0-9- ]+[\"|\']'''
    print(t_STRING.__doc__, t.value)
    t.value = t.value.replace('"','')
    return t  

def t_OPENP(t):
    r'\('
    #print(t_OPENP.__doc__)
    return t

def t_ENDP(t):
    r'\)'
    #print(t_ENDP.__doc__)
    return t

def t_COMMENT(t):
    r'\#.*'
    pass


t_ignore = "\t | \n"

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
import ply.lex as lex;lex.lex()

def p_S(p):
    'S : LCURLY BODY RCURLY'
#    print(p_S.__doc__)
    p[0] = {}
    p[0]['code'] = p[2]['code']
    cleaned_code = pretty_code(p[0]['code'])
    symTbl = str(symbolTable)
    print(symTbl)
    print(cleaned_code)
    f = open("out.txt","w")
    f.write(".DATA \n")
    for index, item in enumerate(symbolTable.table):
        f.write( "  " + str(index) + "  " + str(item["value"]) + "    " + str(item["type"]) + "\n")

    f.write(".CODE \n")
    f.write(cleaned_code)
    f.write("\n.END")
    f.close()

def pretty_code(code):
    out = ''
    for item in code:
        if type(item) == list:
            out += pretty_code(item)
        else:
            out +=  "   " + str(item)
    out = out.split("\n")
    text = ''
    for item in out:
        if item.strip() != '':
            text+=item + '\n'
    
    return text
 
def p_BODY_CODE_BODY(p):
    'BODY : CODE BODY '
    print(p_BODY_CODE_BODY.__doc__)
    p[0] = {}
    p[0]['code'] =  p[1]['code'] + p[2]['code']
    if 'next' in p[1]:
        p[1]['next'].assign_me()
        p[2]['code'].insert(0,p[1]['next'])
    
def p_BODY_CODE(p):
    'BODY : '
    print(p_BODY_CODE.__doc__)
    p[0] = {}
    p[0]['code'] = []

def p_CODE_ID_E(p):
    'CODE : ID ASSIGN E SEMICOLON'
    print(p_CODE_ID_E.__doc__)
    p[0] = {}
    index = symbolTable.add_symbol(p[1])
    p[0]['code'] = [p[3]['code'] , 'MOV, M[%s], M[%s]' % (str(index), str(p[3]['m']) ) , '\n']

def p_CODE_ID_STRING(p):
    'CODE : ID ASSIGN STRING SEMICOLON'
    print(p_CODE_ID_STRING.__doc__)
    symbolTable.add_symbol(p[1], p[3])
    p[0] = {'code':[]}

def p_CODE_IF_ELSE(p):
    'CODE : IF OPENP CON ENDP LCURLY BODY RCURLY ELSE LCURLY BODY RCURLY'
    print(p_CODE_IF_ELSE.__doc__)
    p[0] = {}
    p[0]['next'] = Label.get_empty_label()
    trueLabel = Label.get_new_label()
    falseLabel = Label.get_new_label()
    p[0]['code'] = [ p[3]['code'], '\n', \
    p[3]['type'], trueLabel,'\n' ,'jmp  ' , falseLabel,'\n' ,\
    trueLabel , ':', '\n', p[6]['code'], '\n', 'jmp    ', p[0]['next'], '\n' ,falseLabel , ':', '\n', p[10]['code'], '\n', p[0]['next'], ':', '\n', ]

def p_CODE_WHILE_ELSE(p):
    'CODE : WHILE OPENP CON ENDP LCURLY BODY RCURLY ELSE LCURLY BODY RCURLY'
    print(p_CODE_WHILE_ELSE.__doc__)
    p[0] = {}
    p[0]['next'] = Label.get_empty_label()
    while_label = Label.get_new_label()
    trueLabel = Label.get_new_label()
    falseLabel = Label.get_new_label()
    p[0]['code'] = [
        p[3]['code'], '\n',p[3]['type'], trueLabel,'\n',
        'jmp    ', falseLabel,'\n',
        while_label,':','\n' ,p[3]['code'] , '\n',p[3]['type'], trueLabel,'\n',
        'jmp    ', p[0]['next'],'\n' ,
        trueLabel, ':','\n' ,
        p[6]['code'],'\n',
        'jmp    ', while_label, '\n',
        falseLabel, ':','\n' ,
        p[10]['code'],'\n',
        p[0]['next'], '\n',
    ]


def p_CODE_WHILE_WITHOUT_ELSE(p):
    'CODE : WHILE OPENP CON ENDP LCURLY BODY RCURLY'
    print(p_CODE_WHILE_ELSE.__doc__)
    p[0] = {}
    p[0]['next'] = Label.get_empty_label()
    while_label = Label.get_new_label()
    trueLabel = Label.get_new_label()

    p[0]['code'] = [
        while_label, ':', '\n', p[3]['code'], '\n',p[3]['type'], trueLabel,'\n',
        'jmp    ', p[0]['next'],'\n',
        trueLabel,':' , '\n',
        p[6]['code'],'\n',
        'jmp    ',while_label, '\n',
        p[0]['next'], '\n',
    ]


def p_CODE_E(p):
    'CODE : E SEMICOLON'
    print(p_CODE_E.__doc__)
    p[0] = {}
    p[0]['code'] = p[1]['code']

def p_CON_CMP(p):
    'CON : E CMP E'
    print(p_CON_CMP.__doc__)
    p[0] = {}
    p[0]['code'] = ['cmp M[%s],M[%s]' % (p[1]['m'], p[3]['m']), '\n' ]
    p[0]['type'] = p[2]['type']

def p_CMP_EQUAL(p):
    'CMP : EQUAL'
    print(p_CMP_EQUAL.__doc__)
    p[0] = {}
    p[0]['type'] = 'je '

def p_CMP_NOT_EQUAL(p):
    'CMP : NOTEQUAL'
    print(p_CMP_NOT_EQUAL.__doc__)
    p[0] = {}
    p[0]['type'] = 'jne '


def p_CMP_LESS(p):
    'CMP : LESS'
    print(p_CMP_LESS.__doc__)
    p[0] = {}    
    p[0]['type'] = 'jl '


def p_CMP_BIGGER(p):
    'CMP : BIGGER'
    print(p_CMP_BIGGER.__doc__)
    p[0] = {}
    p[0]['type'] = 'jb '
    

def p_E_E_plus_T(p):
    'E : E PLUS T'
    print(p_E_E_plus_T.__doc__)
    temp = symbolTable.add_new_temp(0)
    if 'type' in p[3] and p[3]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    if 'type' in p[1] and p[1]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    p[0] = {}
    p[0]['code'] = [ p[1]['code'] , p[3]['code']  , 'ADD, M[%s], M[%s], M[%s]' % (str(temp), str(p[1]['m']), str(p[3]['m'])), '\n' ]
    p[0]['m'] = temp

def p_E_E_minus_T(p):
    'E : E MINUS T'
    print(p_E_E_minus_T.__doc__)
    temp = symbolTable.add_new_temp(0)
    if 'type' in p[3] and p[3]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    if 'type' in p[1] and p[1]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    p[0] = {}
    p[0]['code'] = [p[1]['code'] , p[3]['code'] , 'SUB, M[%s], M[%s], M[%s]' % (str(temp), str(p[1]['m']), str(p[3]['m'])), '\n' ]
    p[0]['m'] = temp


def p_E_T(p):
    'E : T'
    print(p_E_T.__doc__)
    p[0] = p[1]

def p_T_T_mul_F(p):
    'T : T MUL F'
    print(p_T_T_mul_F.__doc__)
    print(p[1], p[3])
    if 'type' in p[3] and p[3]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    if 'type' in p[1] and p[1]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    p[0] = {}
    temp = symbolTable.add_new_temp(0)
    p[0]['code'] = [ p[1]['code'] , p[3]['code'] ,
     'MUL, M[%s], M[%s], M[%s]' % (str(temp), str(p[1]['m']), str(p[3]['m'])), '\n' ]
    p[0]['m'] = temp

def p_T_T_div_F(p):
    'T : T DIV F'
#    print(p_T_T_div_F.__doc__)
    if 'type' in p[3] and p[3]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    if 'type' in p[1] and p[1]['type'] in 'printinput':
        raise Exception("you can not use print and input in calculation")

    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = [ p[1]['code'], '\n' , p[3]['code'], '\n' , 'DIV, M[%s], M[%s], M[%s]' % (str(temp), '\n', str(p[1]['m']), '\n', str(p[3]['m'])), '\n' ]
    p[0]['m'] = temp


def p_T_F(p):
    'T : F'
    print(p_T_F.__doc__)
    p[0] = p[1]

def p_F_num(p):
    'F : NUMBER'
    print(p_F_num.__doc__)
    temp = symbolTable.add_new_temp(p[1])
    p[0] = {'m':temp, 'code':[]}

def p_F_num_num_POW(p):
    'F : NUMBER POW NUMBER'
#    print(p_F_num_num_POW.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['POW, M[%s], %s, %s' % (str(temp), str(p[1]), str(p[3])), '\n' ]
    p[0]['m'] = temp

def p_F_id_id_POW(p):
    'F : ID POW ID'
#    print(p_F_id_id_POW.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['POW, M[%s], M[%s], M[%s]' % (str(temp), str(symbolTable.index(p[1])), str(symbolTable.index(p[3]))), '\n' ]
    p[0]['m'] = temp

def p_F_id_num_POW(p):
    'F : ID POW NUMBER'
#    print(p_F_id_num_POW.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = [ 'POW, M[%s], M[%s], %s' % (str(temp), str(symbolTable.index(p[1])), str(p[3])) ]
    p[0]['m'] = temp

def p_number_id_POWr(p):
    'F : NUMBER POW ID'
#    print(p_number_id_POWr.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['POW, M[%s], %s, M[%s]' % (str(temp), str(p[1]) ,str(symbolTable.index(p[3]))) ]
    p[0]['m'] = temp

def p_id_number(p):
    'F : ID'
    print(p_id_number.__doc__)
    print(p[1])
    p[0] = {'m':symbolTable.index(p[1]), 'code':[]}

def p_F_PEP(p):
    'F : OPENP E ENDP'
    print(p_id_number.__doc__)
    p[0] = p[2]

def p_STRING_SUB_ID_0(p):
    'F : ID LBRAC ID COLON ID RBRAC '
    print(p_STRING_SUB_ID_0.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['SUBSTR M[%s],M[%s],M[%s]:M[%s]' % (str(temp), str(p[1]['m']), str(p[3]['m']), str(p[5]['m'])), '\n' ]
    p[0]['m'] = temp
    p[0]['type'] = 'substr'    

def p_STRING_SUB_ID_1(p):
    'F : ID LBRAC  COLON ID RBRAC'
    print(p_STRING_SUB_ID_1.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['SUBSTR M[%s],M[%s],:M[%s]' % (str(temp), str(symbolTable.index(p[1])), str(symbolTable.index(p[4]['m']))), '\n' ]
    p[0]['m'] = temp
    p[0]['type'] = 'substr'

def p_STRING_SUB_ID_2(p):
    'F : ID LBRAC ID COLON RBRAC '
    print(p_STRING_SUB_ID_2.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['SUBSTR M[%s],M[%s],M[%s]:' % (str(temp), str(symbolTable.index(p[1])), p[3]), '\n' ]
    p[0]['m'] = temp
    p[0]['type'] = 'substr'

def p_STRING_SUB_NUM_0(p):
    'F : ID LBRAC NUMBER COLON NUMBER RBRAC'
    print(p_STRING_SUB_NUM_0.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['SUBSTR M[%s],M[%s],%s:%s' % (str(temp), str(symbolTable.index(p[1])), str(int(p[3])), str(int(p[5]))), '\n' ]
    p[0]['m'] = temp
    p[0]['type'] = 'substr'    

def p_STRING_SUB_NUM_1(p):
    'F : ID LBRAC COLON NUMBER RBRAC'
#    print(p_STRING_SUB_NUM_1.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['SUBSTR M[%s],M[%s],:%s' % (str(temp), str(symbolTable.index(p[1])), str(int(p[4])) ), '\n']
    p[0]['m'] = temp
    p[0]['type'] = 'substr'    

def p_STRING_SUB_NUM_2(p):
    'F : ID LBRAC NUMBER COLON RBRAC'
#    print(p_STRING_SUB_NUM_2.__doc__)
    temp = symbolTable.add_new_temp(0)
    p[0] = {}
    p[0]['code'] = ['SUBSTR M[%s],M[%s],%s:' % (str(temp), str(symbolTable.index(p[1])), str(int(p[3]))), '\n' ]
    p[0]['m'] = temp
    p[0]['type'] = 'substr'

def p_F_op_E_ep(p):
    'F : ID OPENP E ENDP'
    print(p_F_op_E_ep.__doc__)
    p[0] = {}
    print(p[1])
    if p[1] == 'print':
        p[0]['code'] = ['PRINT M[%s]' % (str(p[3]['m']), ), '\n' ]
        p[0]['type'] = 'print'
    elif p[1] == 'chop':
        temp = symbolTable.add_new_temp(0)
        p[0]['code'] = [ 'CHOP M[%s] , M[%s]' % (p[3]['m'], temp, ), '\n' ]
        p[0]['m'] = temp
        p[0]['type'] = 'chop'
    elif p[1] == 'str':
        temp = symbolTable.add_new_temp(0)
        p[0]['code'] = [ 'STR M[%s] , M[%s]' % (p[3]['m'], temp, ), '\n' ]
        p[0]['m'] = temp
        p[0]['type'] = 'str'
    elif p[1] == 'len':
        temp = symbolTable.add_new_temp(0)
        p[0]['code'] = [ 'LEN M[%s] , M[%s]' % (p[3]['m'], temp, ), '\n' ]
        p[0]['m'] = temp
        p[0]['type'] = 'len'        
    else:
        raise Exception("just print, chop and str works in this way ")


def p_F_op_STRING_ep(p):
    'F : ID OPENP STRING ENDP'
    print(p_F_op_STRING_ep.__doc__)
    p[0] = {}
    if p[1] == 'input':
        temp = symbolTable.add_new_temp(0)
        p[0]['code'] = [ 'INPUT %s , M[%s]' % (p[3], temp, ), '\n' ]
        p[0]['m'] = temp
        p[0]['type'] = 'input'
    elif p[1] == 'print':
        p[0]['type'] = 'print'        
        p[0]['code'] = [ 'PRINT, '+p[3], '\n' ]
    elif p[1] == 'len':
        temp = symbolTable.add_new_temp(0)
        p[0]['code'] = [ 'LEN %s , M[%s]' % (p[3], temp, ), '\n' ]
        p[0]['m'] = temp
        p[0]['type'] = 'len' 
    else:
        raise Exception("just print and input and len works in this way")
def p_F_op_EMPTY_ep(p):
    'F : ID OPENP ENDP'
    print(p_F_op_EMPTY_ep.__doc__)
    p[0] = {}
    if p[1] == 'input':
        temp = symbolTable.add_new_temp(0)
        p[0]['code'] = ['INPUT, M[%s]' % (temp, ), '\n' ]
        p[0]['m'] = temp
        p[0]['type'] = 'input'

    else:
        raise Exception("just input can work in this way")

def p_F_op_STRING_ID_ep(p):
    'F : ID OPENP STRING COMMA ID ENDP'
    print(p_F_op_STRING_ID_ep.__doc__)
    if p[1] == 'print':
        if symbolTable.exists(p[5]):
            p[0] = {
                'code':['PRINT ' + p[3] + ', ' +'M[' + str(symbolTable.index(p[5])) + ']', '\n' ],
                'type':'print',
            }
        else:
            raise Exception("undefined parameter for print with second parameter")
    else:
        raise Exception("just print can work in this way!")


def p_error(p):
  print("Syntax error at '%s'" % p.value)

import ply.yacc as yacc; yacc.yacc()
#while True:
  #s = input('calc > ')
f = open("input.txt")
s = f.read()
print(s)
yacc.parse(s)
