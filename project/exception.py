class SymbolTableException(Exception):
    pass

class IdNotFound(SymbolTableException):
    pass

class InvalidName(SymbolTableException):
    pass

