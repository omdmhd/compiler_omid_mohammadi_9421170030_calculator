from exception import *

class SymbolTable:
    def __init__(self):
        self.table = []
        self.temp_name_counter = 0

    def add_symbol(self, name, value="None", temp=False):
        if temp == False:
            if name[0:len("temp_name_just_compiler")] == "temp_name_just_compiler":
                raise InvalidName

        for index, item in enumerate(self.table):
            if item['name'] == name:
                item['value'] == value
                item['type'] = type(value)
                return index
        self.table.append({"name":name, "value":value, "type":type(value)})
        return len(self.table) - 1

    def get_type(self, index):
        return self.table[index]

    def change_value(self, index, value):
        self.table[index]['value'] = value
        self.table[index]['type'] = str(type(value))

    def exists(self, name):
        for item in self.table:
            if item['name'] == name:
                return True
        return False
    
    def value(self, name):
        for item in self.table:
            if item['name'] == name:
                return item['value']
        return False
        
    def assign(self, first, second):    
        pass
    def index(self, name):
        for i in range(0, len(self.table)):
            if self.table[i]['name'] == name:
                return i
        raise IdNotFound


    def add_new_temp(self, value):
        name = self.generate_new_temp_name()
        return self.add_symbol(name, value, True)

    def table(self, index):
        return self.table[index]

    def generate_new_temp_name(self):
        name = "temp_name_just_compiler" + str(self.temp_name_counter)
        self.temp_name_counter += 1
        return name
    
    def __str__(self):
        out = ''
        for i in range(0, len(self.table)):
            out += "item %sth, %s, %s, %s" % (str(i) ,self.table[i]['name'], str(self.table[i]['value']), self.table[i]['type'] ) + '\n'
        return out
class Label:
    label = 0
    def __init__(self, number):
        self.number = number
    

    @classmethod
    def get_new_label(cls):
        cls.label += 1
        return cls(cls.label)
    
    @classmethod
    def get_empty_label(cls):
        return cls(None)
    
    def assign_me(self):
        Label.label += 1
        self.number = Label.label

    def __str__(self):
        return 'label-' + str(self.number)
    
    def __repr__(self):
        return 'label-' + str(self.number)

symbolTable = SymbolTable()
