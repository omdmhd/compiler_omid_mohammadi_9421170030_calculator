'''
    MOV R2, 21
    MOV R3, 22
    ADD R23, R3, R2
    ADD R24, R23
    OUT R24 

'''
class Error(BaseException):
    def __init__(self,message):
        self.message = message

class Assembely:
    def __init__(self):
        self.registers =[False] * 32
        self.code = ""
        self.last_reg = -1

    def mov(self, number):
        register_index = self.find_free_register()
        register = str("R" + str(register_index) )
        self.code += "MOV " + register + ", " + str(number) + "\n"
        self.last_reg = register_index
        return register_index
    def operation(self,opp , first, second):
        register_index = self.find_free_register()
        self.last_reg = register_index
        register = str("R" + str(register_index) )
        first_register = str("R" + str(first))
        second_register = str("R" + str(second))
        self.code += opp + " " + register + " , " + first_register + " , " + second_register + "\n"
        self.registers[first] = False
        self.registers[second] = False
        return register_index

    def add(self, first, second):
        return self.operation("ADD", first, second)

    def sub(self, first, second):
        return self.operation("SUB", first, second)

    def mul(self, first, second):
        return self.operation("MUL", first, second)

    def div(self, first, second):
        return self.operation("DIV", first, second)

    def find_free_register(self):
        for index, reg in enumerate(self.registers):
            if reg == False:
                self.registers[index] = True
                return index
        
        raise Error("No Free Register Available")

    def append_output_command(self):
        if self.last_reg != -1:
            self.code += 'OUT R' + str(self.last_reg) 

    def generate_code(self):
        return self.code 