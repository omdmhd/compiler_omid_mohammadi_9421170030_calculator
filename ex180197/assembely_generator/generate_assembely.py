import re
from assembely import Assembely
class Error(BaseException):
    def __init__(self,message):
        self.message = message

class Lexical:
    def __init__(self,string):
        self.current=['$',0]
        self.string = string
        self.index = 0
        self.privious_index = 0
        self.code = Assembely()
    def getToken(self): 
        token=[]
        self.privious_index = self.index;
        if len(self.string) == self.index:
            token=['$',0]
        elif self.string[self.index] in '*/+-()':
            token=[self.string[self.index],0]
        elif '0' <= self.string[self.index] <= '9':
            a=re.match('[0-9]+(\.[0-9]+)?', self.string[self.index:])
            token=['n',float(a.group())]
            self.index += a.end()-1
        else:
            raise Error('lexical error ')
        self.index+=1
        self.current = token
        return token

def E(lex):
    x=T(lex);
    while lex.current[0] in '+-':
        p=lex.current[0]
        lex.getToken()
        y=T(lex)
        if p=='+':
            #x+=y
            x = lex.code.add(x, y)
        else:
            #x-=y;
            x = lex.code.sub(x, y)
    return x

def T(lex):
    x=F(lex);
    while lex.current[0] in '*/':
        p=lex.current[0]
        lex.getToken()
        y=F(lex)
        if p=='*': 
            #x*=y
            x = lex.code.mul(x, y)
        else:      
            x = lex.code.div(x, y)
    return x
    
def F(lex):
    x=0
    if lex.current[0]=='n': 
    #    x=lex.current[1]
        x = lex.code.mov(lex.current[1])
    elif lex.current[0]=='(':
        lex.getToken()
        x=E(lex) 
        if lex.current[0] != ')': 
            raise Error(' ) is missing ')
    else: 
        raise Error(' Error in F ');
    lex.getToken()
    return x

def main():
    s=input('Enter >> ');
    while s!='':
        try:    
            lex=Lexical(s)
            lex.getToken()
            print(E(lex))
            lex.code.append_output_command()
            print(lex.code.generate_code())
            with open("file.asm", "w") as f:
                f.write(lex.code.generate_code())
                
        except Exception as err:
            print(err) 
            print('Error Input')
        s=input('Enter >> ')
main()