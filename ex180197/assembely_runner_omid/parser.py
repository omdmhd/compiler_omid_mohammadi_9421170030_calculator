import re
class Error(BaseException):
    def __init__(self,message):
        self.message = message

class Lexical:
    def __init__(self,string):
        self.current=['$',0]
        self.string = string
        self.index = 0
        self.privious_index = 0
    def getToken(self): 
        token=[]
        self.privious_index = self.index;
        if len(self.string) == self.index:
            token=['$',0]
        elif self.string[self.index] == ' ' or self.string[self.index] == ',' or self.string[self.index] == '\n':
            self.index += 1
            return self.getToken()
        elif self.string[self.index:self.index+3] in ['MOV', 'SUB', 'ADD', 'DIV', 'MUL', 'OUT']:
            token=[self.string[self.index:self.index+3],0]
            self.index+=2
        elif self.string[self.index] == 'R':
            self.index+=1
            # we should not allow bigger than 32
            a=re.match('[0-9]+?', self.string[self.index:])            
            token=['R',int(a.group())]
        elif '0' <= self.string[self.index] <= '9':
            a=re.match('[0-9]+(\.[0-9]+)?', self.string[self.index:])
            token=['n',float(a.group())]
            self.index += a.end()-1
        else:
            raise Error('lexical error ')
        self.index+=1
        self.current = token
        return token

class Processor:
    def __init__(self):
        self.registers = [0] * 32

    def check_reg(self, reg):
        if reg > 31:
            raise Error("Invalid Register")
    def mov(self, reg, value):
        self.check_reg(reg)
        self.registers[reg] = value

    def opp(self, opperation, result_reg, reg1, reg2):
        self.check_reg(result_reg);self.check_reg(reg1);self.check_reg(reg2)
        first_operand = self.registers[reg1]
        second_operand = self.registers[reg2]
        result = 0
        if opperation == 'ADD':
            result = first_operand + second_operand
        elif opperation == 'SUB':
            result = first_operand - second_operand
        elif opperation == 'MUL':
            result = first_operand * second_operand
        elif opperation == 'DIV':
            result = first_operand / second_operand
        else:
            raise Error("Invalid Operation")        
        self.registers[result_reg] = result

    def out(self, reg):
        self.check_reg(reg)
        print(self.registers[reg])

class Runner:
    def __init__(self,lex):
        self.lex = lex
        self.token = lex.getToken()
        self.pc = Processor()

    def get_token(self):
        self.token = self.lex.getToken()
        return self.token

    def start(self):
        if self.token[0] == 'OUT':
            reg = self.R()
            self.pc.out(reg[1])        
        elif self.token[0] == 'MOV':
            self.A()
        elif self.token[0] in ['SUB', 'ADD', 'DIV', 'MUL']:
            opperation  = self.token[0]
            registers = self.B()
            self.pc.opp(opperation, registers[0][1], registers[1][1], registers[2][1])
        else:
            raise Error("Syntax Error")
        
        self.get_token()
        if self.token[0] != '$':
            # need get token
            self.start()

    def A(self):
        Reg = self.R()
        value = self.D()
        self.pc.mov(Reg[1], value[1])

    def D(self):
        value = self.get_token()
        if value[0] != 'n':
            #fix comment
            raise Error('Invalid Value')
        return value

    def R(self):
        reg = self.get_token()
        if reg[0] != 'R' :
            raise Error("Syntax Error")
        return reg        

    def B(self):
        return [self.R(), self.R(), self.R()]


def main():
    with open("../assembely_generator/file.asm") as f:
        out = f.readlines()
        text = "".join(out)
        lex = Lexical(text)
        try:

            run = Runner(lex)
            run.start()
        
        except Exception as err:
            print(err)

        ''' 
            while True:
                token = lex.getToken()
                print(token)
                if token[0] == '$':
                    break;
        '''
main()