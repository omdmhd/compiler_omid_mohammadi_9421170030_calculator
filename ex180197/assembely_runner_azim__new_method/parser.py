import re

with open("../assembely_generator/file.asm", 'r') as myfile:
    data=myfile.read().replace('\n', ',')
    data = re.sub(
           r'\s+', 
           ',', 
           data
        )
    data = re.sub(
           r',+', 
           ',', 
           data
        )
    LCal =  data.split(',')
    Register = {}
    ListT = []
    for i in range(30):
        Register['R'+str(i)] = None
        ListT.append('R'+str(i))
    i = 0
    while i < len(LCal):
        if LCal[i] in ['ADD' , 'MUL' , 'SUB' , 'DIV']:
            if LCal[i] == 'ADD':
                Firstp = Register[LCal[i+1]]
                if LCal[i+2] in ListT:
                    Secondp = Register[LCal[i+2]]
                else:
                    Secondp = float(LCal[i+2])
                if LCal[i+3] in ListT:
                    Thirdp = Register[LCal[i+3]]
                else:
                    Thirdp = float(LCal[i+3])
                Register[LCal[i+1]] = Secondp + Thirdp
            elif LCal[i] == 'MUL':
                Firstp = Register[LCal[i+1]]
                if LCal[i+2] in ListT:
                    Secondp = Register[LCal[i+2]]
                else:
                    Secondp = float(LCal[i+2])
                if LCal[i+3] in ListT:
                    Thirdp = Register[LCal[i+3]]
                else:
                    Thirdp = float(LCal[i+3])
                Register[LCal[i+1]] = Secondp * Thirdp
            elif LCal[i] == 'DIV':
                Firstp = Register[LCal[i+1]]
                if LCal[i+2] in ListT:
                    Secondp = Register[LCal[i+2]]
                else:
                    Secondp = float(LCal[i+2])
                if Register[LCal[i+3]] in ListT:
                    Thirdp = Register[LCal[i+3]]
                else:
                    Thirdp = float(LCal[i+3])
                Register[LCal[i+1]] = Secondp / Thirdp
            elif LCal[i] == 'SUB':
                Firstp = Register[LCal[i+1]]
                if LCal[i+2] in ListT:
                    Secondp = Register[LCal[i+2]]
                else:
                    Secondp = float(LCal[i+2])
                if LCal[i+3] in ListT:
                    Thirdp = Register[LCal[i+3]]
                else:
                    Thirdp = float(LCal[i+3])
                Register[LCal[i+1]] = Secondp - Thirdp
        elif LCal[i] in ['MOV']:
            if LCal[i+2] in ListT:
                Secondp = Register[LCal[i+2]]
            else:
                Secondp = float(LCal[i+2])
            Register[LCal[i+1]] = Secondp
        elif LCal[i] in ['OUT']:
            print(Register[LCal[i+1]])
        i += 1