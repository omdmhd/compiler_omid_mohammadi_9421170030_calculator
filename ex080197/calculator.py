''' 
  RULES:
  S : E
  E : E + T
  E : E - T
  E : T
  T : T * F
  T : T / F
  T : F
  F : a
  F : ( E )

'''

tokens = ('NUMBER', 'PLUS', 'MINUS', 'DIV', 'MUL', 'OPENP', 'ENDP')
t_PLUS    = r'\+'
t_MINUS    = r'\-'
t_DIV    = r'\/'
t_MUL    = r'\*'
t_OPENP    = r'\('
t_ENDP    = r'\)'

def t_NUMBER(t):
	r'[0-9]+'
	t.value = int(t.value)
	return t
t_ignore = " \t"
def t_error(t):
	print("Illegal character '%s'" % t.value[0])
	t.lexer.skip(1)
import ply.lex as lex;lex.lex()

def p_S(p):
	'S : E'
	print(p[1])

def p_E_E_plus_T(p):
	'E : E PLUS T'
	p[0] = p[1] + p[3]

def p_E_E_minus_T(p):
	'E : E MINUS T'
	p[0] = p[1] - p[3]

def p_E_T(p):
	'E : T'
	p[0] = p[1]

def p_T_T_mul_F(p):
	'T : T MUL F'
	p[0] = p[1] * p[3]

def p_T_T_div_F(p):
	'T : T DIV F'
	p[0] = p[1] / p[3]

def p_T_F(p):
	'T : F'
	p[0] = p[1]

def p_F_num(p):
	'F : NUMBER'
	p[0] = p[1]

def p_F_op_E_ep(p):
	'F : OPENP E ENDP'
	print(p[2])
	p[0] = p[2]

def p_error(p):
  print("Syntax error at '%s'" % p.value)

import ply.yacc as yacc; yacc.yacc()
while True:
  s = input('calc > ')
  if s.strip()=='':break # با یک Enter که ورودی خالی باشد برنامه پایان یابد
  yacc.parse(s)
